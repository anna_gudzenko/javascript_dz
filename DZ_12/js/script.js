"use strict";
document.addEventListener("DOMContentLoaded", () => {
  const images = document.querySelectorAll(".image-to-show"),
    div = document.querySelector(".images-wrapper"),
    buttonToStop = document.createElement("button"),
    buttonToContinue = document.createElement("button");

  buttonToStop.textContent = "Stop";
  buttonToContinue.textContent = "Continue";
  buttonToContinue.className = "btn";
  buttonToStop.className = "btn";
  div.prepend(buttonToStop);
  buttonToStop.after(buttonToContinue);
  let i = 3;

  let timer = setInterval(changePictures, 3000);
  function changePictures() {
    images[i].style.display = "none";
    i++;
    if (i === images.length) {
      i = 0;
    }
    images[i].style.display = "block";
  }

  div.addEventListener("click", (evt) => {
    if (evt.target.textContent === "Stop") {
      clearInterval(timer);
    } else if (evt.target.textContent === "Continue") {
      timer = setInterval(changePictures, 3000);
    }
  });
});

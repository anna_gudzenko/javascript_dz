"use strict";
let array = [
  "hello",
  "world",
  "Kiev",
  ["Borispol", "Irpin"],
  "Kharkiv",
  "Odessa",
  "Lviv",
];
let parent = document.body;
function displayAList(array, par) {
  const ul = document.createElement("ul");
  ul.innerHTML = createList(array);
  par.append(ul);
}

function createList(array) {
  return array
    .map((value) => {
      if (Array.isArray(value)) {
        return `<ul>${createList(value)}</ul>`;
      } else {
        return `<li>${value}</li>`;
      }
    })
    .join("");
}

displayAList(array, parent);
function timing(sec) {
  const span = document.createElement("span");
  const deletEl = document.querySelector("ul");
  span.textContent = sec;
  document.body.append(span);
  setInterval(() => {
    span.innerHTML = --sec;
    if (sec === -1) {
      deletEl.remove();
      span.remove();
    }
  }, 1000);
}
timing(3);
/* function displayAList(array, parent) {
  const ul = document.createElement("ul");
  array.forEach((iterator) => {
    if (Array.isArray(iterator)) {
      displayAList(iterator, ul);
      ul.innerHTML = iterator.map((elem) => `<li>${elem}</li>`).join("");
      console.log(iterator);
    } else {
      ul.innerHTML += `<li>${iterator}</li>`;
       ul.innerHTML = array.map((iterator) => `<li>${iterator}</li>`).join("");
    }
    parent.append(ul);
  });
} */

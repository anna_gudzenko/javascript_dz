"use strict";
const tabTitle = document.querySelector(".tabs");
tabTitle.addEventListener("click", (event) => {
  clearedStyleToElement(event);
  const equate = document.querySelector(
    `[data-article=${event.target.dataset.id}]`
  );
  if (equate) {
    equate.classList.add("active-content");
  }
});
function clearedStyleToElement(event) {
  const content = document.querySelectorAll(".content-element");
  content.forEach((iterator) => iterator.classList.remove("active-content"));
  const menuTabs = tabTitle.children;
  for (const el of menuTabs) {
    if (el !== event.target) {
      el.classList.remove("active");
    } else {
      el.classList.add("active");
    }
  }
}

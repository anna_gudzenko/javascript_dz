"use strict";

let number;

do {
  number = prompt("Enter number: ");
} while (isNaN(+number) || number.trim() === "");

function factorial(n) {
  if (n !== 1) {
    return n * factorial(n - 1);
  } else {
    return n;
  }
}
alert(factorial(number));

"use strict";
function createNewUser() {
  const newUser = {
    firstName: "",
    lastName: "",
    birthday: "",
    getLogin() {
      return (
        this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase()
      );
    },
    getAge(birth) {
      const bDay = new Date(birth.split(".", birth.length).reverse().join("-"));
      const now = new Date();
      const misecondsDiff = now - bDay;
      const diffdays = misecondsDiff / 1000 / (60 * 60 * 24);
      const age = Math.floor(diffdays / 365.25);
      return age;
    },
    getPassword() {
      return (
        this.firstName.slice(0, 1).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(6, 10)
      );
    },
  };

  do {
    newUser.firstName = prompt("Your name?");
  } while (validation(newUser.firstName));
  do {
    newUser.lastName = prompt("Your last name?");
  } while (validation(newUser.lastName));
  newUser.birthday = prompt("What is your date of birth?", "dd.mm.yyyy");
  return newUser;
}
function validation(a) {
  return !a || a.trim() === "" || !isNaN(+a);
}
const user = createNewUser();
console.log(user);
console.log(user.getLogin());
console.log(user.getAge(user.birthday));
console.log(user.getPassword());

"use strict";

const baseArray = ["hello", "world", 23, "23", 11, true, null];
function filterBy(arr, type) {
  let newArray = arr.filter((value) => {
    return typeof value !== type && value !== null;
  });
  console.log(newArray);
  return newArray;
}
filterBy(baseArray, "string");

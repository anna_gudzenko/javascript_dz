"use strict";

const form = document.querySelector(".password-form");
const error = document.createElement("p");

form.addEventListener("click", (event) => {
  if (event.target.tagName === "I") {
    const input = event.target.closest(".input-wrapper").childNodes[1];

    if (event.target.classList.contains("fa-eye")) {
      input.setAttribute("type", "text");
      event.target.classList.remove("fa-eye");
      event.target.classList.add("fa-eye-slash");
    } else {
      input.setAttribute("type", "password");
      event.target.classList.remove("fa-eye-slash");
      event.target.classList.add("fa-eye");
    }
  }
  if (event.target.tagName === "BUTTON") {
    const firstValue = document.getElementById("pass").value;
    const secondValue = document.getElementById("confirmpass").value;

    if (
      firstValue === secondValue &&
      firstValue &&
      secondValue &&
      secondValue.trim() !== "" &&
      firstValue.trim() !== ""
    ) {
      alert(`You are welcome`);
      clearValue(event);
    } else {
      error.className = "red-style";
      error.textContent = "Нужно ввести одинаковые значения";
      event.target.before(error);
      clearValue(event);
    }
  }
  if (event.target.tagName === "INPUT") {
    error.remove();
  }
});
function clearValue(event) {
  document.getElementById("pass").value = "";
  document.getElementById("confirmpass").value = "";
  event.preventDefault();
}

"use strict";

const input = document.getElementById("main-input"),
  form = document.querySelector(".form"),
  span = document.createElement("span"),
  button = document.createElement("button"),
  p = document.createElement("p"),
  div = document.createElement("div");

div.classList.add("span-container");

button.addEventListener("click", clearForm);
input.addEventListener("focus", () => input.classList.add("green-border"));
input.addEventListener("click", () => {
  p.remove();
  input.classList.remove("red-border");
  input.value = "";
});
input.addEventListener("blur", priceModification);
function priceModification() {
  input.classList.remove("green-border");
  if (input.value <= 0 || !input.value || isNaN(+input.value)) {
    clearForm();
    input.classList.add("red-border");
    p.textContent = "Please enter correct price";
    input.after(p);
  } else {
    createPriceBlock();
  }
}
function createPriceBlock() {
  form.prepend(div);
  div.prepend(span);
  span.textContent = `Текущая цена: ${input.value}`;
  span.classList.add("span-style");

  button.textContent = "+";
  button.classList.add("button-style");
  span.after(button);
  input.style.color = `rgb(36, 187, 36)`;
}
function clearForm() {
  div.remove();
  input.value = "";
}

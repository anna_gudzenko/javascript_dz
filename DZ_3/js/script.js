"use strict";
let number1;
let number2;
let operation;
let result;
function calc(a, b, c) {
  switch (c) {
    case "+":
      result = +a + +b;
      break;
    case "-":
      result = a - b;
      break;
    case "/":
      result = a / b;
      break;
    case "*":
      result = a * b;
      break;
    case "**":
      result = a ** b;
      break;
  }
  return result;
}
do {
  number1 = prompt("Введите первое число: ", " ");
} while (validation(number1));
do {
  number2 = prompt("Введите второе число: ", " ");
} while (validation(number2));
do {
  operation = prompt("Введите математическую операцию: ", "+");
} while (!operation || operation.trim() === "");

function validation(a) {
  return !a || a.trim() === "" || isNaN(+a);
}

console.log(calc(number1, number2, operation));
